import numpy as np
import nltk
import ssl
import os
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context
nltk.download('stopwords')
import argparse
import gensim
from spellchecker import SpellChecker
from nltk.corpus import stopwords
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
import pyspark
from pyspark.sql import SparkSession
import subprocess
from pyspark.sql.types import StringType, ArrayType
from pyspark.sql.functions import udf
from pyspark.sql.functions import lit,col

def get_similar_sentences(tf_idf_scores, text, model_name, model_dim, code_sent_vectors,code_sent):
    sentences = text.lower().split('.')
    sentences = [sentence.strip() for sentence in sentences if sentence.strip()]
    code_sent = [sentence.strip() for sentence in code_sent if sentence.strip()]
    import nltk
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        pass
    else:
        ssl._create_default_https_context = _create_unverified_https_context
    nltk.download('stopwords')
    from nltk.corpus import stopwords
    cachedStopWords = stopwords.words("english")
    spell = SpellChecker()
    sent_words = [[word.lower() for word in sentence.split() if word not in cachedStopWords and word != ""] for sentence in sentences]
    sent_words = [word for word in sent_words if word]
    if not tf_idf_scores:
      return ""
    scores_list = tf_idf_scores.strip().split(';')
    tf_idf_dict = {i.split('-')[0].lower().strip(): float(i.split('-')[1].strip()) for i in scores_list}
    normalization_factor = sum([value for _, value in tf_idf_dict.items()])
    tf_idf_dict = {i:tf_idf_dict[i]/normalization_factor for i in tf_idf_dict}
    # subprocess.call(f"hadoop fs -copyToLocal {model_hadoop_path} ./", shell=True)
    w2v_model_path = f"./{model_name.split('/')[-1]}"
    assert os.path.exists(w2v_model_path), f"{w2v_model_path} not in {os.listdir('./')}"

    # add split code and if condition
    # w2v_model = Word2Vec.load(w2v_model_path)
    # sent_vectors = []
    # for words in sent_words:
    #     sent_vector = 0
    #     for word in words:
    #         try:
    #             w2v = w2v_model.wv[word]
    #             weighted_w2v = tf_idf_dict[word]*w2v
    #         except:
    #             try:
    #                 corrected_word = spell.correction(word)
    #                 w2v = w2v_model.wv[corrected_word]
    #                 weighted_w2v = tf_idf_dict[corrected_word]*w2v
    #             except:
    #                 weighted_w2v = np.zeros(model_dim)
    #         sent_vector += weighted_w2v
    #     sent_vectors.append(sent_vector.tolist())

    biow2v_model = KeyedVectors.load_word2vec_format(w2v_model_path, binary=True)
    sent_vectors = []
    for words in sent_words:
        sent_vector = 0
        for word in words:
            try:
                w2v = biow2v_model[word]
                weighted_w2v = tf_idf_dict[word]*w2v
            except:
                try:
                    corrected_word = spell.correction(word)
                    w2v = biow2v_model[corrected_word]
                    weighted_w2v = tf_idf_dict[corrected_word]*w2v
                except:
                    weighted_w2v = np.zeros(model_dim)
            sent_vector += weighted_w2v
        sent_vectors.append(sent_vector.tolist())


    sent_vectors = np.array(sent_vectors)
    code_sent_vectors = np.array(code_sent_vectors)
    sent_vectors_norm = np.linalg.norm(sent_vectors, axis = 1)
    code_vectors_norm = np.linalg.norm(code_sent_vectors, axis = 1)
    pairwise_norm = np.outer(sent_vectors_norm, code_vectors_norm)
    pairwise_norm[pairwise_norm==0] = float('inf')
    dot_product = sent_vectors.dot(np.transpose(code_sent_vectors))/(pairwise_norm)
    most_similar_sentence = np.max(dot_product, axis=1)
    most_similar_sentence_matching =  np.argmax(dot_product, axis=1)
    code_sent_matching = [code_sent[i]+f'_{j}' for i,j in zip(most_similar_sentence_matching,most_similar_sentence)]
    sentences = np.array(sentences)[most_similar_sentence>0.75]
    similar_sentences = '|'.join(i for i in sentences)
    return similar_sentences

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--code_sent_vectors_path",type=str,required=True)
    parser.add_argument("--patient_summary_path", type=str, required=True)
    parser.add_argument("--base_path", type=str, required=True)
    parser.add_argument("--model_name", type=str, required=True)
    parser.add_argument("--model_dim", type=str, required=True)
    args = parser.parse_args()
    code_sent_vectors_path = args.code_sent_vectors_path
    patient_summary_path = args.patient_summary_path
    word_vector_model = args.model_name
    word_vector_model_dim = args.model_dim
    base_path = args.base_path

    spark = SparkSession.builder.master("yarn").appName("distributed-clinical-notes-processing").getOrCreate()
    sc = spark.sparkContext

    code_sent_vectors_df = spark.read.format("parquet").option("header", "true").load(code_sent_vectors_path)
    patient_summary_df = spark.read.format("csv").option("header", "true").load(patient_summary_path)
    code_sent = spark.read.load(f"{base_path}/code_sents").select("Code","TEXT")
    condition = ['Code']
    all_data_df = code_sent_vectors_df.join(patient_summary_df, condition, "inner")
    all_data_df = all_data_df.join(code_sent,condition,"inner")

    all_data_df = all_data_df.repartition(1000)
    get_similar_sentence_udf = udf(get_similar_sentences, StringType())
    all_data_df = all_data_df.withColumn("Similar_Sentences", get_similar_sentence_udf('Scores', 'Patient_Summary', 'word_vector_model', 'model_dim', 'Code_Sent_Vectors'))
    all_data_df.write.parquet(f"{base_path}/final_similar_sentences")
