export BASEPATH=/data/01/ts/data/ve2/dqc/mchk/phi/no_gbd/r000/inbound
export CONDAENV=negation_extr
export HADOOP_CONF_DIR=/etc/hadoop/conf

input_path=$1
echo "path to input is  ${input_path}"

spark-submit \
--master yarn \
--deploy-mode cluster \
--queue sps_dcom_yarn \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf spark.yarn.appMasterEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/"  \
--conf spark.executorEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/" \
--conf spark.executorEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf  spark.yarn.maxAppAttempts=0 \
--driver-memory 10g \
--driver-cores 2 \
--conf spark.driver.memoryOverhead=5g \
--conf spark.executor.extraJavaOptions=-XX:+UseParallelGC \
--conf spark.dynamicAllocation.enabled=true \
--executor-memory 15g \
--executor-cores 4 \
--num-executors 15 \
--conf spark.sql.shuffle.partition=200 \
--conf spark.default.parallelism=200 \
--conf spark.scheduler.minRegisteredResourcesRatio=1 \
--conf spark.yarn.executor.memoryOverhead=5g \
--conf spark.sql.broadcastTimeout=36000 \
--conf spark.shuffle.service.enabled=true \
--conf spark.driver.maxResultSize=6g \
--archives "${BASEPATH}/anaconda3/envs/${CONDAENV}.zip#${CONDAENV}_zip" \
--name negation_extraction \
./negation_extraction_spark_submit.py --simi_sent_path ${input_path}
exit_code=$?
if [[ $exit_code -ne 0 ]];then
    echo "Failed"
    exit 1
fi
echo "negation_extraction_spark_submit.py"


#--conf  spark.yarn.maxAppAttempts=0 --driver-memory 20g --driver-cores 2 --conf spark.driver.memoryOverhead=5g --conf spark.executor.extraJavaOptions=-XX:+UseParallelGC --conf spark.dynamicAllocation.enabled=false --executor-memory 20g --executor-cores 3 --num-executors 15 --conf spark.sql.shuffle.partition=200 --conf spark.default.parallelism=200 --conf spark.scheduler.minRegisteredResourcesRatio=1 --conf spark.yarn.executor.memoryOverhead=2g --conf spark.driver.maxResultSize=6g --archives "${BASEPATH}/anaconda3/envs/${CONDAENV}.zip#${CONDAENV}_zip" --py-files ../../nlp_generalised_product/dist/${folder_name}-1.0-py3.7.egg --name distributed-contraxx-processing-ingestConnection-${connection_id} ../../nlp_generalised_product/nlp_scripts_py/ingestConnection.py --connection_id $connection_id --db_user $db_user --db_host $db_host --db_passwd $db_password --db_name $db_name --keytab_filename $keytab_file

