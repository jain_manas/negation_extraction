#!/bin/bash

config_properties_path="${HILABS_ENV_PATH}/config.properties"
source $config_properties_path

export HADOOP_BASE_PATH=$(mysql -u$db_user -p$db_password -h$db_host -D$db_name -se "SELECT cp.value FROM config_properties cp WHERE cp.key=\"seed_path\";")
echo $HADOOP_BASE_PATH

export BASEPATH=/data/01/ts/data/ve2/dqc/mchk/phi/no_gbd/r000/inbound
export CONDAENV=nlp_cda
export HADOOP_CONF_DIR=/etc/hadoop/conf

tf_idf_scores_path=${HADOOP_BASE_PATH}umls/text_tfidf/tfidf_1_final.csv 
codes_data_path=${HADOOP_BASE_PATH}umls/text_tfidf/text_1_final
# model_hadoop_path=/${HADOOP_BASE_PATH}umls/gensim_word2vec.model
model_name=$1
model_dim=$2
echo "path to tfidf_scores is  ${tf_idf_scores_path}"
echo "using ${model_name} embedding model"
echo "dimension of embedding model is ${model_dim}"

spark-submit \
--master yarn \
--deploy-mode cluster \
--queue cdi_cda_yarn \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf spark.yarn.appMasterEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/"  \
--conf spark.executorEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/" \
--conf spark.executorEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf  spark.yarn.maxAppAttempts=0 \
--driver-memory 10g \
--driver-cores 2 \
--conf spark.driver.memoryOverhead=5g \
--conf spark.executor.extraJavaOptions=-XX:+UseParallelGC \
--conf spark.dynamicAllocation.enabled=true \
--executor-memory 15g \
--executor-cores 4 \
--num-executors 30 \
--conf spark.sql.shuffle.partition=200 \
--conf spark.default.parallelism=200 \
--conf spark.scheduler.minRegisteredResourcesRatio=1 \
--conf spark.yarn.executor.memoryOverhead=5g \
--conf spark.sql.broadcastTimeout=36000 \
--conf spark.shuffle.service.enabled=true \
--conf spark.driver.maxResultSize=6g \
--files ./${model_name} \
--archives "${BASEPATH}/anaconda3/envs/${CONDAENV}.zip#${CONDAENV}_zip" \
--name distributed-medical-notes-processing \
./create_code_sent_vectors.py --tf_idf_scores_path ${tf_idf_scores_path} --codes_data_path ${codes_data_path} --model_name ${model_name}  --model_dim ${model_dim}
exit_code=$?
if [[ $exit_code -ne 0 ]];then
    echo "Failed"
    exit 1
fi
echo "create_code_sent_vectors.py"


#--conf  spark.yarn.maxAppAttempts=0 --driver-memory 20g --driver-cores 2 --conf spark.driver.memoryOverhead=5g --conf spark.executor.extraJavaOptions=-XX:+UseParallelGC --conf spark.dynamicAllocation.enabled=false --executor-memory 20g --executor-cores 3 --num-executors 15 --conf spark.sql.shuffle.partition=200 --conf spark.default.parallelism=200 --conf spark.scheduler.minRegisteredResourcesRatio=1 --conf spark.yarn.executor.memoryOverhead=2g --conf spark.driver.maxResultSize=6g --archives "${BASEPATH}/anaconda3/envs/${CONDAENV}.zip#${CONDAENV}_zip" --py-files ../../nlp_generalised_product/dist/${folder_name}-1.0-py3.7.egg --name distributed-contraxx-processing-ingestConnection-${connection_id} ../../nlp_generalised_product/nlp_scripts_py/ingestConnection.py --connection_id $connection_id --db_user $db_user --db_host $db_host --db_passwd $db_password --db_name $db_name --keytab_filename $keytab_file


