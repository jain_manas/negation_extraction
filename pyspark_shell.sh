export BASEPATH=/data/01/ts/data/ve2/dqc/mchk/phi/no_gbd/r000/inbound
export CONDAENV=negation_extr
export HADOOP_CONF_DIR=/etc/hadoop/conf

PYSPARK_DRIVER_PYTHON="${BASEPATH}/anaconda3/envs/${CONDAENV}/bin/python" \
PYSPARK_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
pyspark \
--master yarn \
--deploy-mode client \
--queue sps_dcom_yarn \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf spark.yarn.appMasterEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/"  \
--conf spark.executorEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/" \
--conf spark.executorEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf  spark.yarn.maxAppAttempts=0 \
--driver-memory 10g \
--driver-cores 2 \
--conf spark.driver.memoryOverhead=5g \
--conf spark.executor.extraJavaOptions=-XX:+UseParallelGC \
--conf spark.dynamicAllocation.enabled=true \
--executor-memory 10g \
--executor-cores 2 \
--num-executors 1 \
--conf spark.sql.shuffle.partition=200 \
--conf spark.default.parallelism=200 \
--conf spark.scheduler.minRegisteredResourcesRatio=1 \
--conf spark.yarn.executor.memoryOverhead=5g \
--conf spark.sql.broadcastTimeout=36000 \
--conf spark.shuffle.service.enabled=true \
--conf spark.driver.maxResultSize=6g \
--archives "${BASEPATH}/anaconda3/envs/${CONDAENV}.zip#${CONDAENV}_zip" 
--name Pyspark-shell-$1
