#!/bin/bash
# add the path of the below

# no need to make vectors of code again for the same numerator, denominator codes (as per excel file)
# time_stamp=$1
# tf_idf_scores_path=""
# codes_data_path=""
# model_hadoop_path=""
# model_dim=""

# echo "creating vectors of code"
# sh ./run_get_code_sent_vectors.sh $tf_idf_scores_path $codes_data_path $model_hadoop_path $model_dim
# if [[ $exit_code -ne 0 ]];then
#     echo "run_get_code_sent_vectors.sh Failed"
#     exit 1
# fi

# echo ""
# echo "==============================================================="
# echo "vectors of all codes created"
# echo "==============================================================="
# echo ""
ts=$1

config_properties_path="${HILABS_ENV_PATH}/config.properties"
source $config_properties_path

export HADOOP_BASE_PATH=$(mysql -u$db_user -p$db_password -h$db_host -D$db_name -se "SELECT cp.value FROM config_properties cp WHERE cp.key=\"seed_path\";")
echo $HADOOP_BASE_PATH

hadoop fs -mkdir ${HADOOP_BASE_PATH}umls_$ts
hadoop fs -cp ${HADOOP_BASE_PATH}umls/codes_sent_vectors_1 ${HADOOP_BASE_PATH}umls/gensim_word2vec.model ${HADOOP_BASE_PATH}umls/tfidf_new.csv ${HADOOP_BASE_PATH}umls/code_sents ${HADOOP_BASE_PATH}umls_$ts


echo "Creating the dataset"
sh run_getPatientSummary.sh $ts $HADOOP_BASE_PATH
if [[ $exit_code -ne 0 ]];then
    echo "run_getPatientSummaey.sh Failed"
    exit 1
fi

echo "finding most similar sentences in the clinical notes data"
sh ./run_get_similar_sentences.sh ${HADOOP_BASE_PATH}umls_$ts/codes_sent_vectors_1 ${HADOOP_BASE_PATH}umls_$ts/patient_summary_corrected ${HADOOP_BASE_PATH}umls_$ts ${HADOOP_BASE_PATH}umls_$ts/gensim_word2vec.model
if [[ $exit_code -ne 0 ]];then
    echo "run_get_similar_sentences.sh Failed"
    exit 1
fi

echo ""
echo "==============================================================="
echo "similar sentences part completed"
echo "==============================================================="
echo ""

echo "finding negation in the similar sentences"
sh ./run_negation_extraction.sh ${HADOOP_BASE_PATH}umls_$ts/final_similar_sentences
if [[ $exit_code -ne 0 ]];then
    echo "run_negation_extraction.sh Failed"
    exit 1
fi

echo ""
echo "==============================================================="
echo "negation extraction part completed"
echo "==============================================================="
echo ""



