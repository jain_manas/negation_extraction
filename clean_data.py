import re
import numpy as np
import argparse
import pyspark
import html2text
from pyspark.sql import SparkSession
import subprocess
from pyspark.sql.types import StringType
from pyspark.sql.functions import udf, lit

def clean_sentence(text):
    # text1 = re.sub(r'\\||\|\#|\/|\\r|\\r\\n|\\c|</div><div>|<div>|</div>|<br>|</br>|\?|\*|;|-', '', text)
    # text2 = re.sub(r'\n', '. ', html2text.html2text(text1))
    # text3 = re.sub(r'\||\#|\*|\`', '', text2)
    # text4 = re.sub(r' +', ' ', text3)
    # return re.sub(r'\. \.', '', text4)
    text = text.replace('\\r\\n', ' ').replace('\\c', ' ')
    return text

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--base_path",type=str,required=True)
    parser.add_argument("--ts", type=str, required=True)
    args = parser.parse_args()
    base_path = args.base_path
    ts = args.ts

    spark = SparkSession.builder.master("yarn").appName("nlp-cda").getOrCreate()
    sc = spark.sparkContext

    file_path = f"{base_path}umls_{ts}/patient_summary"
    df = spark.read.load(file_path)
    clean_sentence_udf = udf(clean_sentence, StringType())
    df = df.repartition(1000)
    df = df.withColumn("Text",clean_sentence_udf('NarrativeText'))
    df = df.select("PatientID", "PatientIdentifier", "EncounterIdentifier", "Text")
    df.show(1,False)
    df.write.option("header", "true").mode("overwrite").parquet(f"{file_path}_corrected")
