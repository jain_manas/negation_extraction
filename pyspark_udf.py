from array import _FloatTypeCode
 
if True:
    import numpy as np
    import nltk
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        pass
    else:
        ssl._create_default_https_context = _create_unverified_https_context
    nltk.download('stopwords')
    import argparse
    import gensim
    from spellchecker import SpellChecker
    from nltk.corpus import stopwords
    from gensim.models import Word2Vec
    import pyspark
    from pyspark.sql import SparkSession
    import subprocess
    from pyspark.sql.types import StringType, ArrayType, FloatType
    from pyspark.sql.functions import udf, lit
 
def create_sent_vectors(tf_idf_scores, text, model_name, model_dim):
    sentences = text.lower().split('.')
    import nltk
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        pass
    else:
        ssl._create_default_https_context = _create_unverified_https_context
    nltk.download('stopwords')
    from nltk.corpus import stopwords
    cachedStopWords = stopwords.words("english")
    spell = SpellChecker()
    sent_words = [[word.lower() for word in sentence.split() if word not in cachedStopWords and word != ""] for sentence in sentences]
    scores_list = tf_idf_scores.strip().split(';')
    tf_idf_dict = {i.split('-')[0].lower(): float(i.split('-')[1]) for i in scores_list}
    model_hadoop_path = f"/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/tushar/CDA/{model_name}"
    subprocess.call(f"hadoop fs -copyToLocal {model_hadoop_path} ./", shell=True)
    w2v_model_path = f"./{model_name}"
    w2v_model = gensim.models.KeyedVectors.load_word2vec_format(w2v_model_path, binary=True)
    sent_vectors = []
    for words in sent_words:
        sent_vector = 0
        for word in words:
            try:
                w2v = w2v_model[word]
                weighted_w2v = tf_idf_dict[word]*w2v
            except:
                try:
                    corrected_word = spell.correction(word)
                    w2v = w2v_model[corrected_word]
                    weighted_w2v = tf_idf_dict[corrected_word]*w2v
                except:
                    weighted_w2v = np.zeros(model_dim)
            sent_vector += weighted_w2v
        sent_vectors.append(sent_vector.tolist())
    return sent_vectors
 
if __name__ == "__main__":
 
    parser = argparse.ArgumentParser()
    parser.add_argument("--tf_idf_scores_path",type=str,required=True)
    parser.add_argument("--codes_data_path", type=str, required=True)
    parser.add_argument("--model_name", type=str, required=True)
    parser.add_argument("--model_dim", type=str, required=True)
    args = parser.parse_args()
    tf_idf_scores_path = args.tf_idf_scores_path
    codes_data_path = args.codes_data_path
    word_vector_model = args.model_name
    word_vector_model_dim = args.model_dim
   
    spark = SparkSession.builder.master("yarn").appName("distributed-clinical-notes-processing").getOrCreate()
    sc = spark.sparkContext
if True:
    tf_idf_df = spark.read.format("csv").option("header", "true").load(tf_idf_scores_path)
    all_data_df = spark.read.format("csv").option("header", "true").load(codes_data_path)
    condition = ['Code', 'Code_System']
    all_data_df = all_data_df.join(tf_idf_df, condition, "inner")
 
    all_data_df = all_data_df.withColumn("word_vector_model", lit(word_vector_model)).withColumn("model_dim", lit(word_vector_model_dim))
 
    create_sent_vectors_udf = spark.udf.register("create_sent_vectors_udf", create_sent_vectors, ArrayType(ArrayType(FloatType)))
    all_data_df = all_data_df.withColumn("Code_Sent_Vectors", create_sent_vectors_udf('Scores', 'Description', "word_vector_model", "model_dim"))
 
    predictions_path = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/tushar/CDA/codes_sent_vectors"
    all_data_df.repartition(1).write.option("header", "true").mode("overwrite").parquet(f"{predictions_path}")