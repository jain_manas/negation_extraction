import os
import numpy as np
import nltk
import ssl
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context
nltk.download('stopwords')
import argparse
import gensim
from spellchecker import SpellChecker
from nltk.corpus import stopwords
from gensim.models import Word2Vec
from gensim.models import KeyedVectors
import pyspark
from pyspark.sql import SparkSession
import subprocess
from pyspark.sql.types import StringType, ArrayType, FloatType
from pyspark.sql.functions import udf, lit

def create_sent_vectors(tf_idf_scores, text, model_name, model_dim):
    print(f"Text = {text}")
    if not text:
        print("empty text")
        return [np.zeros(model_dim).tolist()]
    sentences = text.lower().strip().split('.')
    import nltk
    import ssl
    try:
        _create_unverified_https_context = ssl._create_unverified_context
    except AttributeError:
        pass
    else:
        ssl._create_default_https_context = _create_unverified_https_context
    nltk.download('stopwords')
    from nltk.corpus import stopwords
    cachedStopWords = stopwords.words("english")
    spell = SpellChecker()
    sent_words = [[word.lower() for word in sentence.split() if word not in cachedStopWords and word != ""] for sentence in sentences]
    sent_words = [word for word in sent_words if word]
    if not tf_idf_scores:
        print("tf_idf_scores are none, rewturning zeros")
        return [np.zeros(model_dim).tolist()]
    scores_list = tf_idf_scores.strip().split(';')
    try:
        tf_idf_dict = {i.split('-')[0].lower().strip(): float(i.split('-')[1].strip()) for i in scores_list}
        print(f"Tf-idf dict = {tf_idf_dict}")
    except:
        print("returning zeros")
        return [np.zeros(model_dim).tolist()]
    normalization_factor = sum([value for _, value in tf_idf_dict.items()])
    if normalization_factor==0:
        print("normalization factor was zero so returning zeros")
        return [np.zeros(model_dim).tolist()]
    tf_idf_dict = {i:tf_idf_dict[i]/normalization_factor for i in tf_idf_dict}
    # subprocess.call(f"hadoop fs -copyToLocal {model_hadoop_path} ./", shell=True)
    print(f"Working Directory Contents: {os.listdir('./')}")
    w2v_model_path = f"./{model_name}"


    # add code to check if it is .bin file or .model file and give conditions accordingly



    assert os.path.exists(w2v_model_path), f"{w2v_model_path} not in {os.listdir('./')}"
    # if conditional add line 68
    # w2v_model = Word2Vec.load(w2v_model_path)
    # sent_vectors = []
    # for words in sent_words:
    #     sent_vector = np.zeros(model_dim)
    #     for word in words:
    #         try:
    #             w2v = w2v_model.wv[word]
    #             weighted_w2v = tf_idf_dict[word]*w2v
    #         except:
    #             try:
    #                 corrected_word = spell.correction(word)
    #                 w2v = w2v_model.wv[corrected_word]
    #                 weighted_w2v = tf_idf_dict[corrected_word]*w2v
    #             except:
    #                 weighted_w2v = np.zeros(model_dim)
    #         sent_vector += weighted_w2v
    #     sent_vectors.append(sent_vector.tolist())

    biow2v_model = KeyedVectors.load_word2vec_format(w2v_model_path, binary=True)
    print("====model loaded=======")
    sent_vectors = []
    for words in sent_words:
        sent_vector = np.zeros(model_dim)
        for word in words:
            try:
                w2v = biow2v_model[word]
                weighted_w2v = tf_idf_dict[word]*w2v
            except:
                try:
                    corrected_word = spell.correction(word)
                    w2v = biow2v_model[corrected_word]
                    weighted_w2v = tf_idf_dict[corrected_word]*w2v
                except:
                    weighted_w2v = np.zeros(model_dim)
            sent_vector += weighted_w2v
        sent_vectors.append(sent_vector.tolist())

    del biow2v_model
    # print("====model deleted====")
    try:
        print(biow2v_model["disease"])
    except:
        print("model deleted successfully")
    return sent_vectors


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--tf_idf_scores_path",type=str,required=True)
    parser.add_argument("--codes_data_path", type=str, required=True)
    parser.add_argument("--model_name", type=str, required=True)
    parser.add_argument("--model_dim", type=str, required=True)
    args = parser.parse_args()
    tf_idf_scores_path = args.tf_idf_scores_path
    codes_data_path = args.codes_data_path
    word_vector_model = args.model_name
    word_vector_model_dim = int(args.model_dim)
    
    spark = SparkSession.builder.master("yarn").appName("distributed-clinical-notes-processing").getOrCreate()
    sc = spark.sparkContext

    tf_idf_df = spark.read.format("csv").option("header", "true").load(tf_idf_scores_path)
    all_data_df = spark.read.format("parquet").option("header", "true").load(codes_data_path) 
    condition = ['Code', 'Code_type']
    all_data_df = all_data_df.join(tf_idf_df, condition, "inner")
    all_data_df = all_data_df.repartition(200)
    all_data_df = all_data_df.withColumn("word_vector_model", lit(word_vector_model)).withColumn("model_dim", lit(word_vector_model_dim))
    create_sent_vectors_udf = spark.udf.register("create_sent_vectors_udf", create_sent_vectors, ArrayType(ArrayType(FloatType())))
    # create_sent_vectors_udf = udf(lambda z:create_sent_vectors(z),ArrayType(ArrayType(FloatType())))
    all_data_df = all_data_df.repartition(200)
    all_data_df = all_data_df.withColumn("Code_Sent_Vectors", create_sent_vectors_udf('TFIDF_scores', 'TEXT', "word_vector_model", "model_dim")).drop('TEXT')

    predictions_path = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/umls/code_sent_folder/code_sent_1"
    all_data_df.write.option("header", "true").mode("overwrite").parquet(f"{predictions_path}")
   

    #nltk.data.path.append('./')
    # nltk.download('stopwords')
# if True:
#     tf_idf_scores_path = "/pr/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/tfidf_scores_updated.csv"
#     codes_data_path = "/pr/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/code_sents/"
#     word_vector_model = "/pr/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/gensim_word2vec.model"
#     word_vector_model_dim = 300 
#     tf_idf_scores_path = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/umls/tfidf_scores_updated.csv"
#     codes_data_path = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/umls/code_sents"
#     word_vector_model = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/umls/gensim_word2vec.model"
#     word_vector_model_dim = 300 

    
#     subprocess.call(f"hadoop fs -copyToLocal {model_hadoop_path} ./", shell=True)

#     import nltk
#     import ssl
#     try:
#         _create_unverified_https_context = ssl._create_unverified_context
#     except AttributeError:
#         pass
#     else:
#         ssl._create_default_https_context = _create_unverified_https_context
