#!/bin/sh
ts=$1
hadoop_base_path=$2

spark-shell --conf spark.port.maxRetries=100 --queue $spark_queue --driver-memory 5g --driver-cores 2 --conf spark.driver.memoryOverhead=2g --executor-memory 15g --executor-cores 4 --num-executors 15 --conf spark.dynamicAllocation.enabled=false --conf spark.sql.shuffle.partitions=200 --conf spark.default.parallelism=200 --conf hadoop.kms.proxyuser.user1.users=* --conf spark.sql.hive.caseSensitiveInferenceMode="INFER_ONLY" --conf "spark.serializer=org.apache.spark.serializer.KryoSerializer" --conf "spark.executor.extraJavaOptions=-XX:+UseG1GC" --name get-Patient-Summary << EOF

import org.apache.spark.sql.SparkSession
val spark = SparkSession.builder.master("yarn").appName("Getting_Patient_Summary").getOrCreate()
import spark.implicits._  

val df = spark.sql("select * from ts_cdicda1ph_allob_r000_wh.NarrativeTextOutput_stage2 where LoadLogKey = '20211217110942' and Load_Date = '20211217110942'")
df.cache()
df.count()
df.write.mode("overwrite").parquet(s"${hadoop_base_path}umls_$ts/patient_summary")

:quit
EOF

export BASEPATH=/data/01/ts/data/ve2/dqc/mchk/phi/no_gbd/r000/inbound
export CONDAENV=nlp_cda
export HADOOP_CONF_DIR=/etc/hadoop/conf

spark-submit \
--master yarn \
--deploy-mode cluster \
--queue sps_dcom_yarn \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON=./${CONDAENV}_zip/${CONDAENV}/bin/python \
--conf spark.yarn.appMasterEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf spark.yarn.appMasterEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/"  \
--conf spark.executorEnv.LD_LIBRARY_PATH="${BASEPATH}/anaconda3/envs/${CONDAENV}/lib/" \
--conf spark.executorEnv.HOME=./${CONDAENV}_zip/${CONDAENV}/ \
--conf  spark.yarn.maxAppAttempts=0 \
--driver-memory 10g \
--driver-cores 2 \
--conf spark.driver.memoryOverhead=5g \
--conf spark.executor.extraJavaOptions=-XX:+UseParallelGC \
--conf spark.dynamicAllocation.enabled=true \
--executor-memory 15g \
--executor-cores 4 \
--num-executors 15 \
--conf spark.sql.shuffle.partition=200 \
--conf spark.default.parallelism=200 \
--conf spark.scheduler.minRegisteredResourcesRatio=1 \
--conf spark.yarn.executor.memoryOverhead=5g \
--conf spark.sql.broadcastTimeout=36000 \
--conf spark.shuffle.service.enabled=true \
--conf spark.driver.maxResultSize=6g \
--archives "${BASEPATH}/anaconda3/envs/${CONDAENV}.zip#${CONDAENV}_zip" \
--name cleaning_html_data \
./clean_data.py --base_path $hadoop_base_path --ts $ts
exit_code=$?
if [[ $exit_code -ne 0 ]];then
    echo "Failed"
    exit 1
fi
echo "get_similar_sentences_copy.py"

