if True:
    import argparse
    import pyspark
    from pyspark.sql import SparkSession
    import subprocess
    from pyspark.sql.types import StringType, ArrayType, FloatType
    from pyspark.sql.functions import udf, lit
    import spacy
    import scispacy
    from spacy import displacy
    from spacy.matcher import PhraseMatcher
    from spacy.tokens import Span
    from negspacy.negation import Negex
    from negspacy.termsets import termset
    from pyspark.sql.types import IntegerType
    from pyspark.sql.functions import col


def lemmatize(note, nlp):
    doc = nlp(note)
    lemNote = [wd.lemma_ for wd in doc]
    return " ".join(lemNote)

# return true 1 if there is a negation (cases having no entities are considered as negation)
def negation_extraction_udf(list_sent):
    from negspacy.negation import Negex
    from negspacy.termsets import termset
    conda_env_name = "negation_extr"
    nlp_bio_spacy = spacy.load(f"./{conda_env_name}_zip/{conda_env_name}/lib/python3.7/site-packages/en_core_sci_md/en_core_sci_md-0.4.0")
    nlp_ner_spacy = spacy.load(f"./{conda_env_name}_zip/{conda_env_name}/lib/python3.7/site-packages/en_ner_bc5cdr_md/en_ner_bc5cdr_md-0.4.0")
    ts = termset("en_clinical_sensitive")
    ts.add_patterns({"preceding_negations" : ["deny", "refuse", "neither","nor","non"], "following_negations" : ["absent", "deny", "decline"]})
    nlp_ner_spacy.add_pipe("negex",config={"neg_termset":ts.get_patterns(), "chunk_prefix": ["no"], "ent_types":["DISEASE", "CHEMICAL"]}, last=True)
    list_sent = list_sent.split("|")
    temp = 0
    for i in range(len(list_sent)):
        lem_text = lemmatize(list_sent[i], nlp_bio_spacy)
        doc = nlp_ner_spacy(lem_text)
        if doc.ents:
            for e in doc.ents:
                if e._.negex==True:
                    return 1
            temp = 1      
    if temp==1:
        return 0
    else:
        return 1

# /anaconda3/envs/negation_extr/lib/python3.7/site-packages/en_core_sci_md/en_core_sci_md-0.4.0
# /anaconda3/envs/negation_extr/lib/python3.7/site-packages/en_ner_bc5cdr_md/en_ner_bc5cdr_md-0.4.0
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--simi_sent_path",type=str,required=True)
    args = parser.parse_args()
    csv_path = args.simi_sent_path
    spark = SparkSession.builder.master("yarn").appName("distributed_negspacy_parser").getOrCreate()
    sc = spark.sparkContext
    similar_sent_df = spark.read.format("csv").option("header", "true").load("/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/test1.csv")
    # similar_sent_df = spark.read.format("csv").option("header", "true").load("/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/temp_csv_manas_new.csv")

    
    # nlp_bio_spacy_model = spacy.load("en_core_sci_md")
    # nlp_ner_spacy_model = spacy.load("en_ner_bc5cdr_md")
    # function(input_sent, nlp_bio_spacy_model, nlp_ner_spacy_model)
    # nlp_ner_spacy_model.add_pipe("negex", config={"ent_types":["DISEASE", "CHEMICAL"]})

    # similar_sent_df = similar_sent_df.withColumn("Negation", negation_extraction_udf("Sentence", nlp_bio_spacy_model, nlp_ner_spacy_model))
    negation_extraction_udf = spark.udf.register("negation_extraction_udf", negation_extraction_udf, IntegerType())
    similar_sent_df = similar_sent_df.withColumn("Negation", negation_extraction_udf("Sentences"))
    final_df = similar_sent_df.where(col("Negation")==0)
    final_df = final_df.drop(col("Negation")) 

    
# if True:
#     similar_sent_df = spark.read.format("csv").option("header", "true").load("/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/test1.csv")
#     negation_extraction_udf = spark.udf.register("negation_extraction_udf", negation_extraction_udf, IntegerType())
#     similar_sent_df = similar_sent_df.withColumn("Negation", negation_extraction_udf("Sentences"))


#     final_df = similar_sent_df.drop(col("Negation")==1)
#     final_df_main = final_df.drop(col("Negation"))



# def print_directory(input):
#     t = "manas"
#     print(f"${t}_zip")
#     print(os.listdir("./"))
#     return 1
# if True:
#     from pyspark.sql.types import IntegerType
#     similar_sent_df = spark.read.format("csv").option("header", "true").load("/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/test1.csv")
#     print_directory_udf = spark.udf.register("print_directory_udf", print_directory, IntegerType())
#     similar_sent_df = similar_sent_df.withColumn("Negation", print_directory_udf("Sentences"))
#     similar_sent_df.show()



